#!/usr/bin/env python3

import os
import pickle
from datetime import datetime
from math import exp, sqrt
from plotly import graph_objects as go

directory = './data/12-4-2019/'
output_directory = './images/12-4-2019/'
data_files = [
	'data_cache_10000mcs_2n.txt',
	'data_cache_10000mcs_4n.txt',
	'data_cache_10000mcs_6n.txt',
	'data_cache_10000mcs_10n.txt',
	'data_cache_10000mcs_16n.txt',
	# 'data_cache_0.5_to_5.0_by_0.05_10000mcs_20n.txt',
	'data_cache_0.5_to_5.0_by_0.05_11000mcs_24n.txt',
	'data_cache_0.5_to_5.0_by_0.05_10000mcs_32n.txt',
	'data_cache_0.5_to_5.0_by_0.05_10000mcs_40n.txt',
	'data_cache_0.5_to_5.0_by_0.025_10000mcs_45n.txt',
	'data_cache_0.5_to_5.0_by_0.025_10000mcs_50n.txt',
]
available_colors = [
	'RGBA(166,206,227,1.00)',
	'RGBA(31,120,180,1.00)',
	'RGBA(178,223,138,1.00)',
	'RGBA(51,160,44,1.00)',
	'RGBA(251,154,153,1.00)',
	'RGBA(227,26,28,1.00)',
	'RGBA(253,191,111,1.00)',
	'RGBA(255,127,0,1.00)',
	'RGBA(202,178,214,1.00)'
]

available_symbols = [
	'circle',
	'square',
	'triangle-left',
	'diamond',
	'cross',
	'x',
	'star-triangle-up',
	'diamond-tall',
	'star-triangle-down',
	'diamond-wide',
	'triangle-right',
	'asterisk'
]

def initialize_plots():
	default_layout = {'height': 1000, 'width': 1800}
	return {
        'E_vs_T': go.Figure().update_layout(
            **default_layout,
            title="Energy vs Temperature (T)",
            xaxis_title="Temperature (T)",
            yaxis_title="Energy"
        ),
        'M_abs_avg_vs_T': go.Figure().update_layout(
            **default_layout,
            title="Absolute Magnetisation per spin (<|M|>/N) vs Temperature (T)",
            xaxis_title="Temperature (T)",
            yaxis_title="Absolute Magnetisation per spin (<|M|>/N)"
        ),
        'Heat_cap_vs_T': go.Figure().update_layout(
			**default_layout,
			title="Specific Heat Capacity per spin (C/N) vs Temperature (T)",
			xaxis_title="Temperature (T)",
			yaxis_title="Specific Heat Capacity per spin (C/N)"
		),
        'Susceptibility_vs_T': go.Figure().update_layout(
			**default_layout,
			title="Magnetic Susceptibility per spin vs Temperature (T)",
			xaxis_title="Temperature (T)",
			yaxis_title="Magnetic Susceptibility per spin"
		),
        'Susceptibility_prime_vs_T': go.Figure().update_layout(
			**default_layout,
			title="Magnetic Susceptibility per spin (x') vs Temperature (T)",
			xaxis_title="Temperature (T)",
			yaxis_title="Magnetic Susceptibility per spin (x')"
		)
    }

def add_to_plot(figures, data, label, color = 'RGBA(217, 72, 1, 1.00)', symbol='circle'):
	temperatures = [*data]
	default_properties = {
		'line':dict(color=color), 
		'marker':dict(symbol=symbol, size=10),
		'mode':'lines+markers',
		'name':label,
	}

	all_E = [data[temp]['E'] for temp in data]
	figures['E_vs_T'].add_trace(
		go.Scatter(x=temperatures, y=all_E, **default_properties)
	)
	# fig_E_vs_T.update_traces(**default_format)

	m_abs = [data[temp]['M_absolute'] for temp in data]
	figures['M_abs_avg_vs_T'].add_trace(
		go.Scatter(x=temperatures, y=m_abs, **default_properties)
	)

	c_n = [data[temp]['Heat_cap'] for temp in data]
	figures['Heat_cap_vs_T'].add_trace(
		go.Scatter(x=temperatures, y=c_n, **default_properties)
	)

	x = [data[temp]['Susceptibility_x'] for temp in data]
	figures['Susceptibility_vs_T'].add_trace(
		go.Scatter(x=temperatures, y=x, **default_properties)
	)

	x_prime = [data[temp]['Susceptibility_x_prime'] for temp in data]
	figures['Susceptibility_prime_vs_T'].add_trace(
		go.Scatter(x=temperatures, y=x_prime, **default_properties)
	)

def output_plots(plots, target_directory = '/images'):
	for name, plot in plots.items():
		plot.write_image(f'{target_directory}/{name}.png')
		plot.show()

def read_results(file):
	with open(file, 'rb') as f:
		data = pickle.load(f)
	return data

plots = initialize_plots()

for file in data_files:
	i = data_files.index(file)
	color = available_colors[i % len(available_colors)]
	symbol = available_symbols[i % len(available_symbols)]
	# Yeah I'm sorry about that hack job right there
	side = file[file.find('mcs_')+4:file.find('n.txt')]

	label = f'{side} x {side} lattice'
	data = read_results(directory + file)
	add_to_plot(plots, data, label, color, symbol)

output_plots(plots, output_directory)