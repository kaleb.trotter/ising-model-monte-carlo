# Monte Carlo Simulation of a 2D Ising Model
## A project for Statistical Mechanics (PHYS 533)

## Current Method
Use the Monte Carlo approach with Metropolis sampling to simulate the magnetization of a 2D sheet of electron spins at various temperatures. 

## Sources
Jacques Kotze, “Introduction to Monte Carlo Methods for an Ising Model of a Ferromagnet,” ArXiv:0803.0217 [Cond-Mat], March 3, 2008, http://arxiv.org/abs/0803.0217.
Lluis Martinez, “The Ising Model: A Simple Statistical Mechanics Model of Magnetism,” Sepmag (blog), May 28, 2015, https://www.sepmag.eu/blog/ising-model.
R. K Pathria and Paul D Beale, Statistical Mechanics (Amsterdam: Elsevier, 2012).
