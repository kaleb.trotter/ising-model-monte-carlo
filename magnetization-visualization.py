#!/usr/bin/env python3

import os
import json
import progressbar
from plotly import graph_objects as go
from plotly.offline import plot

def number_to_spin(number):
	return '+ ' if number == 1 else '- '

def print_lattice(lattice):
	for row in lattice:
		[print(number_to_spin(electron), end='') for electron in row]
		print('')

frames = []
frame_data = []

max_iterations = 5000

bar = progressbar.ProgressBar(max_value = max_iterations)

# with open('./data/11-30-2019/lattice_data_cache_0.5_3_3n.txt', 'r') as fp:
with open('./data/12-06-2019/lattice_data_cache_1.0_55000_50n.txt', 'r') as fp:
    json_line = fp.readline()
    i = 1
    while i < max_iterations:
        lattice = json.loads(json_line)

        row_range = range(len(lattice))
        col_range = range(len(lattice[0]))

        x = []
        y = []
        z = []

        for row in row_range:
            for col in col_range:
                x.append(col)
                y.append(row)
                z.append(lattice[row][col])
        frames.append(go.Frame(data=[go.Heatmap(x=x, y=y, z=z)]))
        frame_data = {
            'x': x, 'y': y, 'z': z, 
            'colorbar': {
                'title': "Magnetization",
                'titleside': "top",
                'tickmode': "array",
                'tickvals': [1, -1],
                'ticktext': ["Spin Up", "Spin Down"],
                'ticks': "outside"
            },
            'colorscale': [
                [0.0, "rgb(166,206,227)"],
                [1.0, "rgb(31,120,180)"]
            ]
        }
        bar += 1
        i += 1
        json_line = fp.readline()

print(frames)
fig = go.Figure(data=go.Heatmap(**frame_data), frames=frames, layout={
    'showlegend': False,
    'updatemenus': [{
        'type': 'buttons', 
        'showactive': False,
        'y': 1, 
        'x': -0.05, 'xanchor': 'right',
        'yanchor': 'top', 'pad': {'t': 0, 'r': 10},
        'buttons': [{
            'label': 'Play',
            'method': 'animate',
            'args': [
                None,
                {'frame': {'duration': 1, 'redraw': False}, 'transition': {'duration': 0}, 'fromcurrent': True}
            ]
        }]
    }]
})

fig.show()
plot(fig, validate=False, filename=f'test_this_{max_iterations}.html')