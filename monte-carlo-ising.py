#!/usr/bin/env python3

import os
import pickle
import json
from datetime import datetime
from random import random, randint, seed
from math import exp
import progressbar
from numpy import arange
from plotly import graph_objects as go, io

def initialize_lattice(x, y):
	rows = range(y)
	cols = range(x)
	# Use a list comprehension to initialize the lattice array
	return [[1 if round(random()) == 1 else -1 for electron in cols] for row in rows]

def number_to_spin(number):
	return '+ ' if number == 1 else '- '

def print_lattice(lattice):
	for row in lattice:
		[print(number_to_spin(electron), end='') for electron in row]
		print('')

def choose_random_position(lattice):
	row_max_index = len(lattice) - 1
	col_max_index = len(lattice[0]) - 1

	x = randint(0, col_max_index)
	y = randint(0, row_max_index)

	return {"x": x, "y": y}

""" 
Calculate the energy of a postion (x, y) in the given lattice. 
Optional param J for the interaction coupling constant defaults 
to -1 but can be used to calculate the energy change cost (dE)
"""
def energy_at_position(lattice, x, y, J = -1):
	size = len(lattice)
	up = (y + 1) % size
	down = (y - 1) % size
	left = (x - 1) % size
	right = (x + 1) % size

	point_spin = lattice[y][x]
	nearest_neighbor_sum = lattice[y][left] + lattice[y][right] + lattice[up][x] + lattice[down][x]

	return J * point_spin * nearest_neighbor_sum

def change_energy_for_position(l, x, y):
	# Note: pass in a 2 for the interaction coupling constant to 
	# correctly count the dE cost
	return energy_at_position(l, x, y, 2)

def should_flip_spin_energy(dE, temp):
	energy_violates_ergodic = dE < 0
	heat_bath_occured = random() < exp(-dE / temp)

	return energy_violates_ergodic or heat_bath_occured

def total_magnetization(lattice):
	return sum([sum(lattice_row) for lattice_row in lattice])

def total_energy(lattice):
	rows = range(len(lattice))
	cols = range(len(lattice[0]))
	energy_matrix = [[energy_at_position(lattice, x, y) for x in cols] for y in rows]
	return sum([sum(row) for row in energy_matrix]) / 4

def alternative_total_energy(lattice):
	E = 0
	N = len(lattice)
	for i in range(len(lattice)):
		for j in range(len(lattice)):
			S = lattice[j][i]
			WF = lattice[j][(i+1) % N] + lattice[(j+1) % N][i] + lattice[j][(i-1)%N] + lattice[(j-1)%N][i]
			E += -WF*S
	return E / 4

def monte_carlo_simulation(steps, lattice, temp, progress_bar = 0, lattice_save_file = False):
	E = 0
	E_2 = 0
	M = 0
	M_2 = 0
	M_absolute = 0
	lattice_range = range(len(lattice)*len(lattice[0]))

	for step in range(steps):
		for lattice_point_count in lattice_range:
			random_point = choose_random_position(lattice)
			x = random_point['x']
			y = random_point['y']

			dE = change_energy_for_position(lattice, x, y)
			if(should_flip_spin_energy(dE, temp)):
				lattice[y][x] *= -1
			# If there's no progress_bar, then we're just incrementing a useless variable
			progress_bar += 1
		if lattice_save_file and not step % 1:
			json.dump(lattice, lattice_save_file)
			lattice_save_file.write('\n')

		current_energy = total_energy(lattice)
		current_magnetization = total_magnetization(lattice)
		E += current_energy
		E_2 += current_energy ** 2
		M += current_magnetization
		M_2 += current_magnetization ** 2
		M_absolute += abs(current_magnetization)

	return {
		"E": E,
		"E_2": E_2,
		"M": M,
		"M_2": M_2,
		"M_abs": M_absolute
	}

def plot_results(data):
	temperatures = [*data]
	default_format = {
		'mode': 'markers',
		'marker_size': 10,
		'name': 'Lattice side length = ' + str(size),
	}
	default_layout = {
		'height': 1000,
		'width': 1800
	}

	file_suffix = f'{str(monte_carlo_steps)}_{str(size)}n.png'
	target_directory = f'images/{datetime.now().strftime("%m-%d-%Y")}'
	if not os.path.exists(target_directory):
		os.mkdir(target_directory)

	all_E = [data[temp]['E'] for temp in data]
	fig_E_vs_T = go.Figure(data=go.Scatter(x=temperatures, y=all_E))

	fig_E_vs_T.update_traces(
		**default_format,
		marker_color='RGBA(0, 90, 50, 1.00)'
	)
	fig_E_vs_T.update_layout(
		**default_layout,
		title="Energy vs Temperature (T)",
		xaxis_title="Temperature (T)",
		yaxis_title="Energy"
	).write_image(target_directory + '/e_vs_t_' + file_suffix)
	fig_E_vs_T.show()

	m_abs = [data[temp]['M_absolute'] for temp in data]
	fig_M_abs_avg_vs_T = go.Figure(data=go.Scatter(x=temperatures, y=m_abs))

	fig_M_abs_avg_vs_T.update_traces(
		**default_format,
		marker_color='RGBA(8, 69, 148, 1.00)'
	)
	fig_M_abs_avg_vs_T.update_layout(
		**default_layout,
		title="Absolute Magnetisation per spin (<|M|>/N) vs Temperature (T)",
		xaxis_title="Temperature (T)",
		yaxis_title="Absolute Magnetisation per spin (<|M|>/N)"
	).write_image(target_directory + '/mabs_vs_t_' + file_suffix)
	fig_M_abs_avg_vs_T.show()

	c_n = [data[temp]['Heat_cap'] for temp in data]
	fig_Heat_cap_vs_T = go.Figure(data=go.Scatter(x=temperatures, y=c_n))

	fig_Heat_cap_vs_T.update_traces(
		**default_format,
		marker_color='RGBA(217, 72, 1, 1.00)'
	)
	fig_Heat_cap_vs_T.update_layout(
		**default_layout,
		title="Specific Heat Capacity per spin (C/N) vs Temperature (T)",
		xaxis_title="Temperature (T)",
		yaxis_title="Specific Heat Capacity per spin (C/N)"
	).write_image(target_directory + '/cv_vs_t_' + file_suffix)
	fig_Heat_cap_vs_T.show()

def get_cache_file_path():
	file_suffix = f'{str(min_temp)}_to_{str(temp)}_by_{str(change_increment)}_{str(monte_carlo_steps)}mcs_{str(size)}n.txt'
	target_directory = 'data/' + datetime.now().strftime("%m-%d-%Y")

	if not os.path.exists(target_directory):
		os.mkdir(target_directory)

	return target_directory + '/data_cache_' + file_suffix

def write_results(data, file_path):
	with open(file_path, "wb+") as f:
		pickle.dump(data, f)
	return file_path

def read_results(file):
	with open(file, 'rb') as f:
		data = pickle.load(f)
	return data

def run_mc_for_temperatures(lattice, temp_range, save_individual_lattices = False):
	data = {}

	iteration_total = len(temp_range) * (transient_steps * number_points + monte_carlo_steps * number_points)
	bar = progressbar.ProgressBar(max_value = iteration_total)

	for temp in temp_range:
		# Discard the transient results (first transient_steps solutions)
		monte_carlo_simulation(transient_steps, lattice, temp, bar)

		lattice_save_file = False
		if(save_individual_lattices):
			lattice_save_file = open(f'./data/{datetime.now().strftime("%m-%d-%Y")}/lattice_data_cache_{temp}_{monte_carlo_steps}_{len(lattice)}n.txt', 'w+')
		temp_results = monte_carlo_simulation(monte_carlo_steps, lattice, temp, bar, lattice_save_file)
		if(save_individual_lattices):
			lattice_save_file.close()

		# Take the average of the observables
		E_cumulative_squared = temp_results['E_2'] * normalization
		M_cumulative_squared = temp_results['M_2'] * normalization
		M_absolute = temp_results['M_abs'] * normalization
		E_squared = (temp_results['E'] ** 2.0) * sq_normalization
		M_squared = (temp_results['M'] ** 2.0) * sq_normalization
		M_fourth = (temp_results['M'] ** 4.0) * qu_normalization

		data[temp] = {
			"E": temp_results['E'] * normalization,
			"E_unnormalized": temp_results['E'],
			"E_squared": E_squared,
			"M": temp_results['M'] * normalization,
			"M_squared": M_squared,
			"M_fourth": M_fourth,
			"M_absolute": M_absolute,
			# Susceptibility per spin (X)
			"Susceptibility_x": (M_cumulative_squared - M_squared) / temp,
			# Susceptibility per spin (X')
			"Susceptibility_x_prime": (M_cumulative_squared - (M_absolute ** 2.0)) / temp,
			# Heat capacity per spin
			"Heat_cap": (E_cumulative_squared - E_squared) / (temp ** 2.0),
			# Cumulant (U_L)
			"Cumulant": 1 - (M_fourth / (3 * M_squared))
		}

	return data

""" 
Global variables
"""
size = 50					# size of one side of the lattice
number_points = size * size	# number of spin points on the lattice
temp = 5.0					# starting point for the temperature
min_temp = 0.5				# minimum temperature
change_increment = 0.05		# amount to change temperature by in iterations
seed(565656) 				# seed value for random number generator
monte_carlo_steps = 50000	# steps to take in the Monte-Carlo
transient_steps = 1		# steps to eliminate the transient
# normalization for averaging
normalization = 1.0 / (monte_carlo_steps * number_points) 	
# normalization for squared quantities
sq_normalization = normalization * (1.0 / monte_carlo_steps)
# normalization for quartic quantities
qu_normalization = sq_normalization * (1.0 / (monte_carlo_steps * monte_carlo_steps))

lattice = initialize_lattice(size, size)

save_individual_lattices = True

cache = get_cache_file_path()
cache_exists = os.path.exists(cache)

data = read_results(cache) if cache_exists else run_mc_for_temperatures(lattice, arange(min_temp, temp, change_increment), save_individual_lattices)

if not cache_exists:
	write_results(data, cache)

plot_results(data)